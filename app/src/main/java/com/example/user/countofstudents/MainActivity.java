package com.example.user.countofstudents;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Integer countOfStudents = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickButtonAddStudent(View viev) {
        countOfStudents++;
        TextView counterView = (TextView)findViewById(R.id.txt_counter);
        counterView.setText(countOfStudents.toString());
//        counterView.setText(String.format(Locale.getDefault(), "id" ,countOfStudents));
    }
}
